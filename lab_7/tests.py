from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper


class Lab7UnitTest(TestCase):

	def test_model_can_create_friend(self):
		new_friend = Friend.objects.create(friend_name = "Ndus", npm = "12345678996")
		self.assertEquals(Friend.objects.all().count(), 1)

	def test_lab_7_url_exists(self):
		response = Client().get('/lab-7/')
		self.assertEquals(response.status_code, 200)

	def test_lab_7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEquals(found.func, index)

	def test_lab_7_using_index_template(self):
		response = Client().get('/lab-7/')
		self.assertTemplateUsed(response, 'lab_7/lab_7.html')

	def test_lab_7_index_page(self):
		response = Client().get('')

	def test_lab_7_friend_list_url_exists(self):
		response = Client().get('/lab-7/friend-list/')
		self.assertEquals(response.status_code, 200)

	def test_lab_7_friend_list_using_daftar_teman_template(self):
		response = Client().get('/lab-7/friend-list/')
		self.assertTemplateUsed(response, 'lab_7/daftar_teman.html')

	def test_lab_7_add_friend_post_success(self):
		response = Client().post('/lab-7/add-friend/', {'name': "haha", 'npm': '123123123'})
		self.assertEquals(response.status_code, 200)

	def test_lab_7_add_friend_post_fail(self):
		response = Client().post('/lab-7/add-friend/')
		self.assertEqual(response.status_code, 400)

	def test_lab_7_add_friend_get_fail(self):
		response = Client().get('/lab-7/add-friend/')
		self.assertEqual(response.status_code, 400)

	def test_lab_7_delete_friend_post_success(self):
		friend = Friend.objects.create(friend_name = "haha", npm = "123123123")
		response = Client().post('/lab-7/delete-friend/', {'id': friend.id})
		self.assertEqual(Friend.objects.all().count(), 0)

	def test_lab_7_delete_friend_post_fail(self):
		response = Client().post('/lab-7/delete-friend/', {})
		self.assertEqual(response.status_code, 400)

	def test_lab_7_delete_friend_get_fail(self):
		response = Client().get('/lab-7/delete-friend/')
		self.assertEqual(response.status_code, 400)

	def test_lab_7_friend_list_json(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_npm_exists_in_friend_not_exist(self):
		self.assertFalse(npm_exists_in_friend("123123123"))

	def test_lab_7_npm_exists_in_friend_exist(self):
		Friend.objects.create(friend_name = "haha", npm = "123123123")
		self.assertTrue(npm_exists_in_friend("123123123"))

	def test_lab_7_model_to_dict(self):
		friend = Friend(friend_name="haha", npm="123123123")
		self.assertTrue("friend_name" in model_to_dict(friend))

	def test_lab_7_validate_npm_ok(self):
		response = Client().post('/lab-7/validate-npm/', {'npm': "123123123"})
		self.assertTrue('false' in response.content.decode('utf8'))

	def test_lab_7_validate_npm_not_ok(self):
		Friend.objects.create(friend_name="haha", npm="123123123")
		response = Client().post('/lab-7/validate-npm/', {'npm': "123123123"})
		self.assertTrue('true' in response.content.decode('utf8'))

	def test_lab_7_validate_npm_fail(self):
		response = Client().get('/lab-7/validate-npm/')
		self.assertEqual(response.status_code, 400)

	def test_lab_7_csui_helper_initiate(self):
		csui_helper = CSUIhelper()
		self.assertTrue(csui_helper.instance is not None)

	def test_lab_7_csui_helper_get_mahasiswa_list(self):
		csui_helper = CSUIhelper()
		mahasiswa_list, prev_exists, next_exists = csui_helper.instance.get_mahasiswa_list(page=5)
		self.assertTrue(mahasiswa_list is not None and prev_exists and next_exists)

	def test_lab_7_get_access_token(self):
		csui_helper = CSUIhelper()
		self.assertTrue(csui_helper.instance.get_access_token() is not None)

	def test_lab_7_get_client_id(self):
		csui_helper = CSUIhelper()
		self.assertTrue(csui_helper.instance.get_client_id() is not None)

	def test_lab_7_get_auth_param_dict(self):
		csui_helper = CSUIhelper()
		dict = csui_helper.instance.get_auth_param_dict()
		self.assertTrue('access_token' in dict and 'client_id' in dict)

	def test_lab_7_invalid_credentials(self):
		csui_helper = CSUIhelper()
		csui_helper.instance.test_change_credentials()
		with self.assertRaises(Exception):
			csui_helper.instance.get_access_token()
