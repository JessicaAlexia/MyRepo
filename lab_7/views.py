from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import json

response = {}

def index(request):
	page = request.GET.get('page', 1)
	csui_helper = CSUIhelper()
	mahasiswa_list, has_prev, has_next = csui_helper.instance.get_mahasiswa_list(page)

	friend_list = Friend.objects.all()
	response = {"mahasiswa_list": mahasiswa_list,
				"friend_list": friend_list,
				"has_prev" : has_prev,
				"has_next" : has_next,
				"page" : page}
	html = 'lab_7/lab_7.html'
	return render(request, html, response)


def friend_list(request):
	html = 'lab_7/daftar_teman.html'
	return render(request, html, response)


@csrf_exempt
def add_friend(request):
	if request.method == 'POST':
		name = request.POST.get('name', None)
		npm = request.POST.get('npm', None)
		if name is None or npm is None or npm_exists_in_friend(npm):
			return HttpResponseBadRequest()

		friend = Friend(friend_name=name, npm=npm)
		friend.save()
		data = model_to_dict(friend)
		return HttpResponse(data)
	return HttpResponseBadRequest()


@csrf_exempt
def delete_friend(request):
	if request.method == 'POST':
		friend_id = request.POST.get('id', None)
		if friend_id is None:
			return HttpResponseBadRequest()

		Friend.objects.filter(id=friend_id).delete()
		return HttpResponse()
	return HttpResponseBadRequest()


@csrf_exempt
def friend_list_json(request):
	friends = Friend.objects.all()
	data = [{'id': friend.id, 'friend_name': friend.friend_name, 'npm': friend.npm} for friend in friends]
	return HttpResponse(json.dumps(data))


@csrf_exempt
def validate_npm(request):
	npm = request.POST.get('npm', None)
	if npm is None:
		return HttpResponseBadRequest()
	data = {'is_taken': npm_exists_in_friend(npm)}
	return JsonResponse(data)


def npm_exists_in_friend(npm):
	return False if not npm else Friend.objects.filter(npm=npm).exists()


def model_to_dict(obj):
	data = serializers.serialize('json', [obj,])
	struct = json.loads(data)
	data = json.dumps(struct[0]["fields"])
	return data
