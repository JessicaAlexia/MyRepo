from django.conf.urls import url
from .views import index

#url for app, https://jessicaalexia.herokuapp.com/lab-2/

urlpatterns = [
	url(r'^$', index, name='index'),
]
