from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.urls import reverse

from .custom_auth import auth_login, auth_logout
from .csui_helper import get_access_token

import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class Lab10UnitTest(TestCase):
	def test_lab_10_url_is_exist(self):
		response = Client().get('/lab-10/')
		self.assertEqual(response.status_code, 200)

	def test_lab_10_page_when_user_is_logged_in_or_not(self):
		#not logged in, render login template
		response = self.client.get('/lab-10/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('lab_10/login.html')

		#logged in, redirect to profile page
		username = env("SSO_USERNAME")
		password = env("SSO_PASSWORD")
		response = self.client.post('/lab-10/custom_auth/login/', {'username': username, 'password': password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/')
		self.assertEqual(response.status_code, 302)
		self.assertTemplateUsed('lab_10/dashboard.html')

	def test_direct_access_to_dashboard_url(self):
		#not logged in, redirect to login page
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 302)

		#logged in, render profile template
		username = env("SSO_USERNAME")
		password = env("SSO_PASSWORD")
		response = self.client.post('/lab-10/custom_auth/login/', {'username': username, 'password': password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-10/dashboard/')
		self.assertEqual(response.status_code, 200)

	def test_logout(self):
		username = env("SSO_USERNAME")
		password = env("SSO_PASSWORD")
		response = self.client.post('/lab-10/custom_auth/login/', {'username': username, 'password': password})
		self.assertEqual(response.status_code, 302)
		response = self.client.post('/lab-10/custom_auth/logout/')
		html_response = self.client.get('/lab-10/').content.decode('utf-8')
		self.assertEqual(response.status_code, 302)

	def test_add_watch_later_and_list_watch_later(self):
		# test jika id yang ditambahkan tidak valid (saat penambahan secara manual)
		response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tidakada'}))
		self.assertEqual(response_post.status_code, 302)

		# test jika menambahkan dengan login (data disimpan di database)
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.post(reverse('lab-10:auth_login'),
										 {'username': self.username, 'password': self.password})
		response_post = self.client.get(reverse('lab-10:dashboard'))
		response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt1396484'}))
		self.assertEqual(response_post.status_code, 302)
		response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
		html_response = response_post.content.decode('utf8')
		
		# test list_watch_later dengan login (data diambil dari database)
		response_post = self.client.get(reverse('lab-10:list_watch_later'))
		self.assertEqual(response_post.status_code, 200)
		self.assertTemplateUsed(response_post, "lab_10/watch_later.html")
		html_response = response_post.content.decode('utf8')
		
		# test add_watched_movie but not on database
		response_post = self.client.get(reverse('lab-10:add_watched_movie', kwargs={'id': 'tt3300542'}))
		self.assertEqual(response_post.status_code, 302)
		response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt3300542'}))
		html_response = response_post.content.decode('utf8')
		
		# test add_watched_movie
		response_post = self.client.get(reverse('lab-10:add_watched_movie', kwargs={'id': 'tt1396484'}))
		self.assertEqual(response_post.status_code, 302)
		response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
		html_response = response_post.content.decode('utf8')

		# test list_watched_movie dengan login (data diambil dari database)
		response_post = self.client.get(reverse('lab-10:list_watched_movie'))
		self.assertEqual(response_post.status_code, 200)
		self.assertTemplateUsed(response_post, "lab_10/watched_movie.html")
		html_response = response_post.content.decode('utf8')
		
		# test jika id yang sama ditambahkan kembali secara manual dengan login
		response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt1396484'}))
		self.assertEqual(response_post.status_code, 302)
		response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
		html_response = response_post.content.decode('utf8')
		
		# menambahkan satu movie lagi dengan login
		response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt3874544'}))
		self.assertEqual(response_post.status_code, 302)
		response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt3874544'}))
		html_response = response_post.content.decode('utf8')
		
		# logout
		response_post = self.client.post(reverse('lab-10:auth_logout'))

		# test jika menambahkan tanpa login (data akan disimpan di session)
		response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt1396484'}))
		self.assertEqual(response_post.status_code, 302)
		response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
		html_response = response_post.content.decode('utf8')

		# test masukan ke watchedmovies tanpa login
		response_post = self.client.get(reverse('lab-10:add_watched_movie', kwargs={'id': 'tt1396484'}))
		self.assertEqual(response_post.status_code, 302)
		
		# test list_watch_later tanpa login (data diambil dari session)
		response_post = self.client.get(reverse('lab-10:list_watch_later'))
		self.assertEqual(response_post.status_code, 200)
		self.assertTemplateUsed(response_post, "lab_10/watch_later.html")
		html_response = response_post.content.decode('utf8')
		
		# test jika id yang sama ditambahkan kembali secara manual tanpa login
		response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt1396484'}))
		self.assertEqual(response_post.status_code, 302)
		response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
		html_response = response_post.content.decode('utf8')
		
		# menambahkan satu movie lagi tanpa login
		response_post = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt4649466'}))
		self.assertEqual(response_post.status_code, 302)
		response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt4649466'}))
		html_response = response_post.content.decode('utf8')
		
		# jika sudah menambahkan namun belum login, maka setelah login movie dari session yang belum ada di database
		# akan disimpan di dalam database
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.post(reverse('lab-10:auth_login'),
										 {'username': self.username, 'password': self.password})
		response_post = self.client.get(reverse('lab-10:dashboard'))

		# logout
		response_post = self.client.post(reverse('lab-10:auth_logout'))

	def test_list_movie_page_exist(self):
		# login
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.post(reverse('lab-10:auth_login'),
										 {'username': self.username, 'password': self.password})
		response_post = self.client.get(reverse('lab-10:movie_list'))
		self.assertEqual(response_post.status_code, 200)
		self.assertTemplateUsed(response_post, "lab_10/list.html")
		response_post = self.client.get(reverse('lab-10:movie_list'), {'judul': 'It', 'tahun': '2017'})
		self.assertEqual(response_post.status_code, 200)
		self.assertTemplateUsed(response_post, "lab_10/list.html")

		# logout
		response_post = self.client.post(reverse('lab-10:auth_logout'))

	def test_detail_page(self):
		# test jika tidak login (tidak ada key 'user_login' di session)
		response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
		self.assertEqual(response_post.status_code, 200)
		self.assertTemplateUsed(response_post, "lab_10/detail.html")
		# login
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.post(reverse('lab-10:auth_login'),
										 {'username': self.username, 'password': self.password})
		response_post = self.client.get(reverse('lab-10:dashboard'))
		response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt1396484'}))
		self.assertEqual(response_post.status_code, 200)
		self.assertTemplateUsed(response_post, "lab_10/detail.html")
		# logout
		response_post = self.client.post(reverse('lab-10:auth_logout'))

	def test_api_search_movie(self):
		# init search
		response = Client().get('/lab-10/api/movie/-/-/')
		self.assertEqual(response.status_code, 200)

		# search movie by title
		response = Client().get('/lab-10/api/movie/justice/-/')
		self.assertEqual(response.status_code, 200)

		# search movie by title and year
		response = Client().get('/lab-10/api/movie/justice/2016/')
		self.assertEqual(response.status_code, 200)

		# 0 > number of result <= 3
		response = Client().get('/lab-10/api/movie/Guardians of Galaxy/2016/')
		self.assertEqual(response.status_code, 200)

		# not found
		response = Client().get('/lab-10/api/movie/lalalalalalaland/-/')
		self.assertEqual(response.status_code, 200)
		
	#csui_helper.py
	def test_invalid_sso_raise_exception(self):
		username = "username"
		password = "password"
		with self.assertRaises(Exception) as context:
			get_access_token(username, password)
		self.assertIn(username, str(context.exception))
