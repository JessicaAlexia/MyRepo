from django.apps import AppConfig


class AppmodelsConfig(AppConfig):
    name = 'AppModels'
