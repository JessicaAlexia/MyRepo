from django.shortcuts import render

# Create your views here.
response = {'author':"Jessica Alexia Jaury"}
def index(request):
    return render(request, 'lab_6/lab_6.html', response)
